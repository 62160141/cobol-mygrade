       IDENTIFICATION DIVISION.
       PROGRAM-ID. GRADE.
       AUTHOR. RENU.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE  HIGH-VALUE.
           05 SUBJECT-ID      PIC X(6).
           05 SUBJECT-NAME    PIC X(50).
           05 SUBJECT-UNIT    PIC 9.
           05 GRADE           PIC X(2).

       FD  AVG-GRADE-FILE.
       01  AVG-GRADE-DETAIL.
           05 AVG-GRADE    PIC 9(1)V9(3).
           05 AVG-TYPE     PIC 9(1)V9(3).
           05 AVG-GRADES   PIC 9(1)V9(3).

       WORKING-STORAGE SECTION.
       01  TEMP-GRADE      PIC 9(3)V9(3).
       01  SUM-GRADE      PIC 9(3)V9(3).
       01  SUM-SCI-GRADE  PIC 9(3)V9(3).
       01  SUM-CS-GRADE   PIC 9(3)V9(3).
       01  SUM-UNIT     PIC 999 VALUE ZEROS.
       01  SUM-SCI-UNIT PIC 999 VALUE ZEROS.
       01  SUM-CS-UNIT  PIC 999 VALUE ZEROS.
       01  COUNT-COM-SCI-UNIT PIC X.
       01  COUNT-ALL   PIC X(2).
           

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-GRADE-FILE 
           PERFORM  UNTIL END-OF-GRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
                 PERFORM 002-AVG THRU 002-EXIT
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
            END-PERFORM 
           CLOSE AVG-GRADE-FILE 
           CLOSE GRADE-FILE
           GOBACK  
           .

           

       001-PROCESS.
           EVALUATE TRUE
              WHEN GRADE IS EQUAL TO "A " 
              MOVE 4 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "B+" 
              MOVE 3.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "B " 
              MOVE 3 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "C+" 
              MOVE 2.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "C " 
              MOVE 2 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "D+" 
              MOVE 1.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "D " 
              MOVE 1 TO TEMP-GRADE 
              WHEN OTHER  MOVE 0 TO TEMP-GRADE
           END-EVALUATE

           MOVE SUBJECT-ID TO COUNT-COM-SCI-UNIT
           MOVE SUBJECT-ID TO COUNT-ALL 
           COMPUTE SUM-UNIT = SUM-UNIT + SUBJECT-UNIT 
           COMPUTE SUM-GRADE = SUM-GRADE + (TEMP-GRADE * SUBJECT-UNIT)
           IF COUNT-COM-SCI-UNIT = "3" THEN
              COMPUTE SUM-SCI-UNIT = SUM-SCI-UNIT + SUBJECT-UNIT
              COMPUTE SUM-SCI-GRADE = SUM-SCI-GRADE 
                + (TEMP-GRADE * SUBJECT-UNIT) 
                IF COUNT-ALL = "31" THEN
                  COMPUTE SUM-CS-UNIT = SUM-CS-UNIT + SUBJECT-UNIT 
                  COMPUTE SUM-CS-GRADE = SUM-CS-GRADE 
                  + (TEMP-GRADE * SUBJECT-UNIT)
                END-IF
           END-IF 
           .
       
       001-EXIT.
           EXIT
           .
       002-AVG.
           COMPUTE AVG-GRADE    = SUM-GRADE / SUM-UNIT  
           COMPUTE AVG-TYPE     = SUM-SCI-GRADE / SUM-SCI-UNIT  
           COMPUTE AVG-GRADES   = SUM-CS-GRADE  / SUM-CS-UNIT
           DISPLAY "AVG                 : " AVG-GRADE
           DISPLAY "AVG SCI             : " AVG-TYPE 
           DISPLAY "AVG COM-SCI         : " AVG-GRADES 
           .
       002-EXIT.
           EXIT
           .

